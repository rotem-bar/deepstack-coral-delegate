import { FastifyInstance } from 'fastify';

import { detectionController } from './deepstack/detection_controller.js';
import { sceneController } from './deepstack/scene_controller.js';
import { faceController } from './deepstack/face_controller.js';

export let deepstackController = async (app: FastifyInstance) => {
	await app.register(detectionController, {
		prefix: '/v1/vision/detection',
	});

	await app.register(sceneController, {
		prefix: '/v1/vision/scene',
	});

	await app.register(faceController, {
		prefix: '/v1/vision/face',
	});
};
