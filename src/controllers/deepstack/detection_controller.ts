import { FastifyInstance } from 'fastify';
import { z } from 'zod';
import { zodToJsonSchema } from 'zod-to-json-schema';
import sharp from 'sharp';

import { detectObjects } from '../../models/object_detection.js';
import { getImageExtension } from '../../common/image.js';

let deepstackDetectionRequestBodySchema = z.object({
	image: z.object({
		_buf: z.instanceof(Buffer),
	}),
	min_confidence: z
		.object({
			value: z.coerce.number(),
		})
		.optional(),
});

type DeepstackDetectionRequestBody = z.infer<typeof deepstackDetectionRequestBodySchema>;

let deepstackDetectionResponseBodySchema = z.object({
	success: z.boolean(),
	predictions: z.array(
		z.object({
			label: z.string(),
			confidence: z.number().min(0).max(100),
			y_min: z.number().min(0),
			y_max: z.number().min(0),
			x_min: z.number().min(0),
			x_max: z.number().min(0),
		})
	),
});

type DeepstackDetectionResponseBody = z.infer<typeof deepstackDetectionResponseBodySchema>;

type DeepstackDetectionResponseBodyResult = DeepstackDetectionResponseBody['predictions'][0];

export let detectionController = async (app: FastifyInstance) => {
	app.post<{ Body: DeepstackDetectionRequestBody }>(
		'/',
		{
			schema: {
				body: zodToJsonSchema(deepstackDetectionRequestBodySchema),
				response: {
					200: zodToJsonSchema(deepstackDetectionResponseBodySchema),
				},
			},
		},
		async (request, reply) => {
			let image = sharp(request.body.image._buf);
			await image.toFile(`./dist/object_detection.${await getImageExtension(image)}`);

			let results = await detectObjects(image);

			let minConfidence = request.body.min_confidence?.value ?? 0.45;
			let deepstackResults: DeepstackDetectionResponseBodyResult[] = results
				.filter((result) => result.score >= minConfidence)
				.map((result) => ({
					label: result.label,
					confidence: result.score,
					y_min: result.boundingBox.y.min,
					y_max: result.boundingBox.y.max,
					x_min: result.boundingBox.x.min,
					x_max: result.boundingBox.x.max,
				}));
			console.log({
				results: results.length,
				resultsPostConfidenceCheck: deepstackResults.length,
				minConfidence,
			});

			let response: DeepstackDetectionResponseBody = {
				success: true,
				predictions: deepstackResults,
			};
			return reply.status(200).send(response);
		}
	);
};
