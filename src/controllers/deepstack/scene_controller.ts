import { FastifyInstance } from 'fastify';
import sharp from 'sharp';
import { z } from 'zod';
import { zodToJsonSchema } from 'zod-to-json-schema';
import assert from 'assert';

import { classifyImage } from '../../models/image_classification.js';

let deepstackSceneRequestBodySchema = z.object({
	image: z.object({
		_buf: z.instanceof(Buffer),
	}),
});

type DeepstackSceneRequestBody = z.infer<typeof deepstackSceneRequestBodySchema>;

let deepstackSceneResponseBodySchema = z.object({
	success: z.boolean(),
	label: z.string(),
	confidence: z.number().min(0).max(100),
});

type DeepstackSceneResponseBody = z.infer<typeof deepstackSceneResponseBodySchema>;

export let sceneController = async (app: FastifyInstance) => {
	app.post<{ Body: DeepstackSceneRequestBody }>(
		'/',
		{
			schema: {
				body: zodToJsonSchema(deepstackSceneRequestBodySchema),
				response: {
					200: zodToJsonSchema(deepstackSceneResponseBodySchema),
				},
			},
		},
		async (request, reply) => {
			let image = sharp(request.body.image._buf);

			let results = await classifyImage(image);
			assert(results.length > 0, 'failed to classify image');

			let topResult = results.sort((a, b) => b.score - a.score)[0];
			assert(topResult, 'failed to get top result of classify image');
			console.log({
				results: results.length,
				topResult,
			});

			let response: DeepstackSceneResponseBody = {
				success: true,
				label: topResult.label,
				confidence: topResult.score,
			};
			return reply.status(200).send(response);
		}
	);
};
