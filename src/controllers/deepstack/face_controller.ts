import { FastifyInstance } from 'fastify';
import { z } from 'zod';
import { zodToJsonSchema } from 'zod-to-json-schema';
import sharp from 'sharp';
import { Tensor4D } from '@tensorflow/tfjs-node';

import { detectFaces } from '../../models/face_detection.js';
import { extractImageByBoundingBox, getImageExtension, getImageTensor } from '../../common/image.js';
import {
	FaceDescriptor,
	createFaceDescriptorByImage,
	createFaceDescriptors,
	deleteStoredFaceDescriptors,
	findClosestFaceDescriptor,
	storeFaceDescriptor,
} from '../../common/face_matching.js';

let deepstackFaceRequestBodySchema = z.object({
	image: z.object({
		_buf: z.instanceof(Buffer),
	}),
	min_confidence: z
		.object({
			value: z.coerce.number(),
		})
		.optional(),
});
type DeepstackFaceRequestBody = z.infer<typeof deepstackFaceRequestBodySchema>;

let deepstackFaceResponseBodySchema = z.object({
	success: z.boolean(),
	predictions: z.array(
		z.object({
			confidence: z.number().min(0).max(1),
			y_min: z.number().min(0),
			y_max: z.number().min(0),
			x_min: z.number().min(0),
			x_max: z.number().min(0),
		})
	),
});
type DeepstackFaceResponseBody = z.infer<typeof deepstackFaceResponseBodySchema>;
type DeepstackFaceResponseBodyResult = DeepstackFaceResponseBody['predictions'][0];

let deepstackFaceRegisterRequestBodySchema = z.object({
	image: z.object({
		_buf: z.instanceof(Buffer),
	}),
	userid: z.object({
		value: z.string(),
	}),
});
type DeepstackFaceRegisterRequestBody = z.infer<typeof deepstackFaceRegisterRequestBodySchema>;

let deepstackFaceRegisterResponseBodySchema = z.object({
	success: z.boolean(),
});
type DeepstackFaceRegisterResponseBody = z.infer<typeof deepstackFaceRegisterResponseBodySchema>;

let deepstackFaceDeleteRequestBodySchema = z.object({
	userid: z.object({
		value: z.string(),
	}),
});
type DeepstackFaceDeleteRequestBody = z.infer<typeof deepstackFaceDeleteRequestBodySchema>;

let deepstackFaceDeleteResponseBodySchema = z.object({
	success: z.boolean(),
});
type DeepstackFaceDeleteResponseBody = z.infer<typeof deepstackFaceDeleteResponseBodySchema>;

let deepstackFaceRecognizeRequestBodySchema = z.object({
	image: z.object({
		_buf: z.instanceof(Buffer),
	}),
	min_confidence: z
		.object({
			value: z.coerce.number(),
		})
		.optional(),
});
type DeepstackFaceRecognizeRequestBody = z.infer<typeof deepstackFaceRecognizeRequestBodySchema>;

let deepstackFaceRecognizeResponseBodySchema = z.object({
	success: z.boolean(),
	predictions: z.array(
		z.object({
			userid: z.string(),
			confidence: z.number().min(0).max(1),
			y_min: z.number().min(0),
			y_max: z.number().min(0),
			x_min: z.number().min(0),
			x_max: z.number().min(0),
		})
	),
});
type DeepstackFaceRecognizeResponseBody = z.infer<typeof deepstackFaceRecognizeResponseBodySchema>;
type DeepstackFaceRecognizeResponseBodyResult = DeepstackFaceRecognizeResponseBody['predictions'][0];

export let faceController = async (app: FastifyInstance) => {
	app.post<{ Body: DeepstackFaceRequestBody }>(
		'/',
		{
			schema: {
				body: zodToJsonSchema(deepstackFaceRequestBodySchema),
				response: {
					200: zodToJsonSchema(deepstackFaceResponseBodySchema),
				},
			},
		},
		async (request, reply) => {
			let image = sharp(request.body.image._buf);
			await image.toFile(`./dist/face_recognition_register.${await getImageExtension(image)}`);

			let results = await detectFaces(image);

			let minConfidence = request.body.min_confidence?.value ?? 0.45;
			let deepstackResults: DeepstackFaceResponseBodyResult[] = results
				.filter((result) => result.score >= minConfidence)
				.map((result) => ({
					confidence: result.score,
					y_min: result.boundingBox.y.min,
					y_max: result.boundingBox.y.max,
					x_min: result.boundingBox.x.min,
					x_max: result.boundingBox.x.max,
				}));
			console.log({
				results: results.length,
				resultsPostConfidenceCheck: deepstackResults.length,
				minConfidence,
			});

			let response: DeepstackFaceResponseBody = {
				success: true,
				predictions: deepstackResults,
			};
			return reply.status(200).send(response);
		}
	);

	app.post<{ Body: DeepstackFaceRegisterRequestBody }>(
		'/register',
		{
			schema: {
				body: zodToJsonSchema(deepstackFaceRegisterRequestBodySchema),
				response: {
					200: zodToJsonSchema(deepstackFaceRegisterResponseBodySchema),
				},
			},
		},
		async (request, reply) => {
			let faceDescriptors: FaceDescriptor[] = [];

			let image = sharp(request.body.image._buf);
			await image.toFile(`./dist/face_recognition_register.${await getImageExtension(image)}`);

			let faceDescriptor = await createFaceDescriptorByImage(image);
			faceDescriptors.push(faceDescriptor);

			for (let faceDescriptor of faceDescriptors) {
				faceDescriptor.label = request.body.userid.value;
				await storeFaceDescriptor(faceDescriptor);
			}

			let response: DeepstackFaceRegisterResponseBody = {
				success: true,
			};
			return reply.status(200).send(response);
		}
	);

	app.post<{ Body: DeepstackFaceDeleteRequestBody }>(
		'/delete',
		{
			schema: {
				body: zodToJsonSchema(deepstackFaceDeleteRequestBodySchema),
				response: {
					200: zodToJsonSchema(deepstackFaceDeleteResponseBodySchema),
				},
			},
		},
		async (request, reply) => {
			await deleteStoredFaceDescriptors(request.body.userid.value);

			let response: DeepstackFaceDeleteResponseBody = {
				success: true,
			};
			return reply.status(200).send(response);
		}
	);

	app.post<{ Body: DeepstackFaceRecognizeRequestBody }>(
		'/recognize',
		{
			schema: {
				body: zodToJsonSchema(deepstackFaceRecognizeRequestBodySchema),
				response: {
					200: zodToJsonSchema(deepstackFaceRecognizeResponseBodySchema),
				},
			},
		},
		async (request, reply) => {
			let image = sharp(request.body.image._buf);
			await image.toFile(`./dist/face_recognition_recognize.${await getImageExtension(image)}`);

			let results = await detectFaces(image.clone());

			let minConfidence = request.body.min_confidence?.value ?? 0.67;
			let confidentResults = results.filter((result) => result.score >= minConfidence);

			let faces: Tensor4D[] = [];
			for (let result of confidentResults) {
				let faceImage = await extractImageByBoundingBox(image, result.boundingBox);
				let face = await getImageTensor(faceImage);
				console.log(face);
				faces.push(face);
			}

			let faceDescriptors = await createFaceDescriptors(faces);
			console.log(faceDescriptors);

			let deepstackResults: DeepstackFaceRecognizeResponseBodyResult[] = [];
			for (let i = 0; i < confidentResults.length; i++) {
				let result = confidentResults[i];

				let confidence = 0;

				let label = 'unknown';
				if (faceDescriptors[i]) {
					let faceDescriptor = findClosestFaceDescriptor(faceDescriptors[i]);
					if (faceDescriptor && faceDescriptor.distance >= minConfidence) {
						label = faceDescriptor.label;
						confidence = faceDescriptor.distance;
					}
				}

				deepstackResults.push({
					userid: label,
					confidence,
					y_min: result.boundingBox.y.min,
					y_max: result.boundingBox.y.max,
					x_min: result.boundingBox.x.min,
					x_max: result.boundingBox.x.max,
				});
			}
			console.log({
				results: results.length,
				resultsPostConfidenceCheck: deepstackResults.length,
				minConfidence,
			});

			let response: DeepstackFaceRecognizeResponseBody = {
				success: true,
				predictions: deepstackResults,
			};
			return reply.status(200).send(response);
		}
	);
};
