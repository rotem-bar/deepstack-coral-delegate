import assert from 'node:assert';
import { Sharp } from 'sharp';

import { Prediction, TensorData, getPredictionData } from '../common/prediction.js';
import { ModelType } from '../common/config.js';
import { getModelLabels } from '../common/labels.js';
import { getModel, getModelInputDimensions } from '../common/model.js';
import { getImageDimensions, getImageTensor } from '../common/image.js';
import { BoundingBox, createBoundingBox, scaleBoundingBox } from '../common/bounding_box.js';

let getObjectDetectionData = (data: TensorData[]) => {
	assert.equal(data.length, 4, 'model must have 4 tensors with data');

	let classIds: TensorData;
	let scores: TensorData;
	let boundingBoxes: TensorData;
	let count: number;

	if (data[3]?.length === 1) {
		classIds = data[1];
		scores = data[2];
		boundingBoxes = data[0];
		count = data[3][0];
	} else if (data[2]?.length === 1) {
		classIds = data[3];
		scores = data[0];
		boundingBoxes = data[1];
		count = data[2][0];
	} else {
		throw new Error('unknown object detection data layout');
	}
	assert.equal(typeof count, 'number', 'count is not a number');
	assert.equal(classIds.length, count, 'class ids length not equal to count');
	assert.equal(scores.length, count, 'scores length not equal to count');
	assert.equal(boundingBoxes.length, count * 4, 'bounding boxes length not equal to count * 4');
	return {
		classIds,
		scores,
		boundingBoxes,
		count,
	};
};

interface ObjectDetectionResult {
	classId: number;
	score: number;
	boundingBox: BoundingBox;
}

let getObjectDetectionResults = async (prediction: Prediction) => {
	let predictionData = await getPredictionData(prediction);
	let objectDetectionData = getObjectDetectionData(predictionData);

	let results = new Array<ObjectDetectionResult>(objectDetectionData.count);
	for (let i = 0; i < results.length; i++) {
		let classId = objectDetectionData.classIds[i];
		let score = objectDetectionData.scores[i];
		let boundingBox = createBoundingBox(objectDetectionData.boundingBoxes.slice(i * 4, i * 4 + 4));
		results[i] = {
			classId,
			score,
			boundingBox,
		};
	}
	return results;
};

export type ObjectDetectionProcessedResult = {
	label: string;
	score: number;
	boundingBox: BoundingBox;
};

export let detectObjects = async (image: Sharp): Promise<ObjectDetectionProcessedResult[]> => {
	let modelType: ModelType = 'object_detection';

	let model = await getModel({
		modelType,
	});

	let labels = await getModelLabels({
		modelType,
	});

	let imageDimensions = await getImageDimensions(image);
	let modelDimensions = getModelInputDimensions(modelType);

	let input = await getImageTensor(
		image.resize({
			...modelDimensions,
			fit: 'fill',
		})
	);
	let prediction = model.predict(input);

	let results = await getObjectDetectionResults(prediction);
	return results.map((result) => {
		let label = labels[result.classId] || 'n/a';
		return {
			label,
			score: result.score,
			boundingBox: scaleBoundingBox(result.boundingBox, imageDimensions),
		};
	});
};
