import { Sharp } from 'sharp';
import assert from 'node:assert';

import { Prediction, TensorData, getPredictionData } from '../common/prediction.js';
import { ModelType } from '../common/config.js';
import { getImageTensor } from '../common/image.js';
import { getModelLabels } from '../common/labels.js';
import { getModel, getModelInputDimensions } from '../common/model.js';

let getImageClassificationData = (data: TensorData[]) => {
	assert.equal(data.length, 1, 'model must have 1 tensor with data');
	let scores = data[0];
	return { scores };
};

interface ImageClassificationResult {
	score: number;
}

let getImageClassificationResults = async (prediction: Prediction): Promise<ImageClassificationResult[]> => {
	let predictionData = await getPredictionData(prediction);
	let imageClassificationData = getImageClassificationData(predictionData);

	let results = new Array<ImageClassificationResult>(imageClassificationData.scores.length);
	for (let i = 0; i < results.length; i++) {
		results[i] = {
			score: imageClassificationData.scores[i],
		};
	}
	return results;
};

interface ImageClassificationProcessedResult {
	label: string;
	score: number;
}

export let classifyImage = async (image: Sharp): Promise<ImageClassificationProcessedResult[]> => {
	let modelType: ModelType = 'image_classification';

	let model = await getModel({
		modelType,
	});
	console.log(Object.keys(model.inputs[0]));

	let labels = await getModelLabels({
		modelType,
	});

	let { width: modelWidth, height: modelHeight } = getModelInputDimensions(modelType);

	let input = await getImageTensor(
		// TODO: add quantization in addition to normalisation
		image
			.resize({
				width: modelWidth,
				height: modelHeight,
			})
			.normalise()
	);
	let prediction = model.predict(input);

	let results = await getImageClassificationResults(prediction);
	return results.map((result, index) => {
		let label = labels[index] || 'n/a';
		return {
			label,
			score: result.score,
		};
	});
};
