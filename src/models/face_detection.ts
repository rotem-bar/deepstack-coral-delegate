import assert from 'node:assert';
import { Sharp } from 'sharp';

import { Prediction, TensorData, getPredictionData } from '../common/prediction.js';
import { ModelType } from '../common/config.js';
import { getModel, getModelInputDimensions } from '../common/model.js';
import { getImageDimensions, getImageTensor } from '../common/image.js';
import { BoundingBox, createBoundingBox, scaleBoundingBox } from '../common/bounding_box.js';

let getFaceDetectionData = (data: TensorData[]) => {
	assert.equal(data.length, 4, 'model must have 4 tensors with data');

	let classifications: TensorData;
	let scores: TensorData;
	let boundingBoxes: TensorData;
	let count: number;

	if (data[3]?.length === 1) {
		boundingBoxes = data[0];
		scores = data[2];
		classifications = data[1];
		count = data[3][0];
	} else {
		throw new Error('unknown face detection data layout');
	}

	assert.equal(typeof count, 'number', 'count is not a number');
	assert.equal(boundingBoxes.length, scores.length * 4, 'bounding boxes length not equal to count * 4');
	assert.equal(classifications.length, scores.length, 'classifications length not equal to scores length');

	return {
		classifications,
		scores,
		boundingBoxes,
		count,
	};
};

interface FaceDetectionResult {
	score: number;
	boundingBox: BoundingBox;
}

let getFaceDetectionResults = async (prediction: Prediction) => {
	let predictionData = await getPredictionData(prediction);
	let faceDetectionData = getFaceDetectionData(predictionData);

	let results = new Array<FaceDetectionResult>(faceDetectionData.count);
	for (let i = 0; i < results.length; i++) {
		let score = faceDetectionData.scores[i];
		let boundingBox = createBoundingBox(faceDetectionData.boundingBoxes.slice(i * 4, i * 4 + 4));
		results[i] = {
			score,
			boundingBox,
		};
	}
	return results;
};

export type ObjectDetectionProcessedResult = {
	score: number;
	boundingBox: BoundingBox;
};

export let detectFaces = async (image: Sharp): Promise<ObjectDetectionProcessedResult[]> => {
	let modelType: ModelType = 'face_detection';

	let model = await getModel({
		modelType,
	});

	let imageDimensions = await getImageDimensions(image);
	let modelDimensions = getModelInputDimensions(modelType);

	let input = await getImageTensor(
		image.resize({
			...modelDimensions,
			fit: 'fill',
		})
	);
	let prediction = model.predict(input);

	let results = await getFaceDetectionResults(prediction);
	return results.map((result) => {
		return {
			score: result.score,
			boundingBox: scaleBoundingBox(result.boundingBox, imageDimensions),
		};
	});
};
