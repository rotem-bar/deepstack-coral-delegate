import { FaceMatcher } from '@vladmandic/face-api';

let _faceMatcher: FaceMatcher;

let loadFaceMatcher = async () => {
	let data = [] as any[];
	return new FaceMatcher(data);
};

export let getFaceMatcher = async () => {
	if (!_faceMatcher) {
		_faceMatcher = await loadFaceMatcher();
	}
	return _faceMatcher;
};
