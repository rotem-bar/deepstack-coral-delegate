import fastify from 'fastify';
import fastifyMultipart from '@fastify/multipart';

import { deepstackController } from './controllers/deepstack_controller.js';

interface CreateApiOptions {
	host: string;
	port: number;
}

export let createApi = async (options: CreateApiOptions) => {
	let app = fastify();

	await app.register(fastifyMultipart, {
		attachFieldsToBody: true,
	});

	await app.register(deepstackController);

	let address = await app.listen({
		host: options.host,
		port: options.port,
	});
	console.log({ address });
};
