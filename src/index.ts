import '@tensorflow/tfjs-node';
import * as faceapi from '@vladmandic/face-api';

import dotenv from 'dotenv';
dotenv.config();

import { createApi } from './api.js';
import { getApiHost, getApiPort } from './common/config.js';
import { loadFaceDescriptorStorage } from './common/face_matching.js';

let main = async () => {
	await loadFaceDescriptorStorage();

	await faceapi.nets.faceRecognitionNet.loadFromDisk('./models/face_recognition');

	await createApi({
		host: getApiHost(),
		port: getApiPort(),
	});
};

main();
