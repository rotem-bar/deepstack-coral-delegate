import * as tf from '@tensorflow/tfjs-node';
import assert from 'node:assert';
import { Sharp } from 'sharp';
import { BoundingBox, getBoundingBoxDimensions, getSafeBoundingBoxDimensions } from './bounding_box.js';

export let getImageTensor = async (image: Sharp) => {
	let { data, info } = await image.removeAlpha().raw().toBuffer({
		resolveWithObject: true,
	});

	let shape: [number, number, number, number] = [1, info.height, info.width, info.channels];

	return tf.tidy(() => {
		return tf.tensor4d(data, shape, 'bool');
	});
};

export type ImageDimensions = {
	width: number;
	height: number;
};

export let getImageDimensions = async (image: Sharp): Promise<ImageDimensions> => {
	let metadata = await image.metadata();
	let { width, height } = metadata;
	assert(width !== undefined, 'failed to extract image width from metadata');
	assert(width > 0, 'image width must be greater than 0');
	assert(height !== undefined, 'failed to extract image height from metadata');
	assert(height > 0, 'image height must be greater than 0');
	return {
		width,
		height,
	};
};

export let extractImageByBoundingBox = async (image: Sharp, boundingBox: BoundingBox) => {
	let imageDimensions = await getImageDimensions(image);
	let boundingBoxDimensions = getSafeBoundingBoxDimensions(boundingBox, imageDimensions);
	return image.clone().extract({
		...boundingBoxDimensions,
		top: boundingBox.y.min,
		left: boundingBox.x.min,
	});
};

export let getImageExtension = async (image: Sharp): Promise<string> => {
	let metadata = await image.metadata();
	return metadata.format || 'png';
};
