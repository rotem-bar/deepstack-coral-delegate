import { DataTypeMap, NamedTensorMap, NumericDataType, Tensor } from '@tensorflow/tfjs-node';

export type Prediction = Tensor | Tensor[] | NamedTensorMap;

export type TensorData = DataTypeMap[NumericDataType];

export let getPredictionData = async (prediction: Prediction): Promise<TensorData[]> => {
	if (prediction instanceof Tensor) {
		return [await prediction.data()];
	}
	let data: TensorData[] = [];
	if (Array.isArray(prediction)) {
		for (let tensor of prediction) {
			data.push(await tensor.data());
		}
	} else {
		let keys = Object.keys(prediction).sort((a, b) => prediction[a].id - prediction[b].id);
		for (let key of keys) {
			data.push(await prediction[key].data());
		}
	}
	return data;
};
