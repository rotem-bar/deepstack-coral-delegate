import { tensor, Tensor } from '@tensorflow/tfjs-node';
import { ImageDimensions } from './image.js';
import { TensorData } from './prediction.js';

export type BoundingBox = {
	x: {
		min: number;
		max: number;
	};
	y: {
		min: number;
		max: number;
	};
};

let scaleBoundingBoxValue = (value: number, scale: number) => {
	return Math.abs(Math.ceil(value * scale));
};

export let scaleBoundingBox = (
	boundingBox: BoundingBox,
	// scale: BoundingBoxScale,
	imageDimensions: ImageDimensions
): BoundingBox => {
	return {
		x: {
			min: Math.min(scaleBoundingBoxValue(boundingBox.x.min, imageDimensions.width), imageDimensions.width),
			max: Math.min(scaleBoundingBoxValue(boundingBox.x.max, imageDimensions.width), imageDimensions.width),
		},
		y: {
			min: Math.min(scaleBoundingBoxValue(boundingBox.y.min, imageDimensions.height), imageDimensions.height),
			max: Math.min(scaleBoundingBoxValue(boundingBox.y.max, imageDimensions.height), imageDimensions.height),
		},
	};
};

export let createBoundingBox = (data: TensorData): BoundingBox => {
	return {
		x: {
			min: data[1],
			max: data[3],
		},
		y: {
			min: data[0],
			max: data[2],
		},
	};
};

export let createScaledBoundingBox = (data: TensorData, imageDimensions: ImageDimensions): BoundingBox => {
	let boundingBox = createBoundingBox(data);
	return scaleBoundingBox(boundingBox, imageDimensions);
};

export type BoundingBoxDimensions = {
	width: number;
	height: number;
};

export let getBoundingBoxDimensions = (boundingBox: BoundingBox): BoundingBoxDimensions => {
	return {
		width: boundingBox.x.max - boundingBox.x.min,
		height: boundingBox.y.max - boundingBox.y.min,
	};
};

export let getSafeBoundingBoxDimensions = (
	boundingBox: BoundingBox,
	imageDimensions: ImageDimensions
): BoundingBoxDimensions => {
	let boundingBoxDimensions = getBoundingBoxDimensions(boundingBox);
	console.log({ nonSafeBoundingBoxDimensions: boundingBoxDimensions, imageDimensions });
	return {
		width: Math.min(boundingBoxDimensions.width, Math.max(imageDimensions.width - boundingBox.x.min, 0)),
		height: Math.min(boundingBoxDimensions.height, Math.max(imageDimensions.height - boundingBox.y.min, 0)),
	};
};

export let getBoundingBoxTensor = (boundingBox: BoundingBox) => {
	return tensor([boundingBox.y.min, boundingBox.x.min, boundingBox.y.max, boundingBox.x.max]);
};
