import { CoralDelegate } from 'coral-tflite-delegate';
import { loadTFLiteModel } from 'tfjs-tflite-node';
import { ModelType, getInferenceType, getModelInputHeight, getModelInputWidth, getModelPath } from './config.js';
import assert from 'node:assert';

export type TFLiteModel = Awaited<ReturnType<typeof loadModel>>;

let _loadedModels: Map<ModelType, TFLiteModel> = new Map();

interface LoadModelOptions {
	path: string;
}

export let loadModel = async (options: LoadModelOptions) => {
	let delegates = [];
	if (getInferenceType() === 'edgetpu') {
		delegates.push(new CoralDelegate());
	}
	return loadTFLiteModel(options.path, {
		delegates,
	});
};

interface GetModelOptions {
	modelType: ModelType;
}

export let getModel = async (options: GetModelOptions) => {
	if (!_loadedModels.has(options.modelType)) {
		let path = getModelPath(options.modelType);
		console.log(`loading model - model type: ${options.modelType}, path: ${path}`);
		let model = await loadModel({
			path,
		});
		_loadedModels.set(options.modelType, model);
		return model;
	}
	let model = _loadedModels.get(options.modelType);
	assert(model, 'failed to load model');
	return model;
};

export let getModelInputDimensions = (modelType: ModelType) => {
	return {
		width: getModelInputWidth(modelType),
		height: getModelInputHeight(modelType),
	};
};
