import assert from 'node:assert';

export type InfereceType = 'cpu' | 'edgetpu';

export type ModelType = 'object_detection' | 'image_classification' | 'face_detection';

let _defaultInferenceType: InfereceType = 'cpu';

export let getInferenceType = (): InfereceType => {
	let inferenceType = process.env.INFERENCE_TYPE as InfereceType | undefined;
	if (!inferenceType) {
		inferenceType = _defaultInferenceType;
	}
	return inferenceType;
};

export let getModelPath = (modelType: ModelType): string => {
	let key = `${modelType.toUpperCase()}_MODEL_PATH`;
	let modelPath = process.env[key];
	assert(modelPath, `no model path specified (${key})`);
	return modelPath;
};

export let getModelLabelsPath = (modelType: ModelType): string => {
	let key = `${modelType.toUpperCase()}_MODEL_LABELS_PATH`;
	let modelLabelsPath = process.env[key];
	assert(modelLabelsPath, `no model labels path specified (${key})`);
	return modelLabelsPath;
};

export let getModelInputWidth = (modelType: ModelType): number => {
	let key = `${modelType.toUpperCase()}_MODEL_INPUT_WIDTH`;
	let modelInputWidth = Number(process.env[key]);
	assert(modelInputWidth > 0, `model input width must be greater than 0 (${key})`);
	return modelInputWidth;
};

export let getModelInputHeight = (modelType: ModelType): number => {
	let key = `${modelType.toUpperCase()}_MODEL_INPUT_HEIGHT`;
	let modelInputHeight = Number(process.env[key]);
	assert(modelInputHeight > 0, `model input height must be greater than 0 (${key})`);
	return modelInputHeight;
};

let _defaultApiPort = 5000;

export let getApiPort = (): number => {
	let apiPort = Number(process.env.API_PORT);
	if (Number.isNaN(apiPort) || apiPort < 1 || apiPort > 65535) {
		apiPort = _defaultApiPort;
	}
	return apiPort;
};

let _defaultApiHost = '0.0.0.0';

export let getApiHost = (): string => {
	let apiHost = process.env.API_HOST;
	if (!apiHost) {
		apiHost = _defaultApiHost;
	}
	return apiHost;
};
