import fs from 'node:fs/promises';
import assert from 'node:assert';
import { ModelType, getModelLabelsPath } from './config.js';

let _loadedModelLabels: Map<ModelType, string[]> = new Map();

interface LoadModelLabelsOptions {
	path: string;
}

export let loadModelLabels = async (options: LoadModelLabelsOptions) => {
	let data = await fs.readFile(options.path, {
		encoding: 'utf-8',
	});
	assert(data, 'failed to read model labels data');

	let split = data.split(/\r?\n/).filter((line) => line.length > 0);
	assert(split.length > 0, 'no valid model labels in data');

	return split;
};

interface GetModelLabelsOptions {
	modelType: ModelType;
}

export let getModelLabels = async (options: GetModelLabelsOptions) => {
	if (!_loadedModelLabels.has(options.modelType)) {
		let path = getModelLabelsPath(options.modelType);
		console.log(`loading model labels - model type: ${options.modelType}, path: ${path}`);
		let labels = await loadModelLabels({
			path,
		});
		_loadedModelLabels.set(options.modelType, labels);
		return labels;
	}
	let labels = _loadedModelLabels.get(options.modelType);
	assert(labels, 'failed to load model labels');
	return labels;
};
