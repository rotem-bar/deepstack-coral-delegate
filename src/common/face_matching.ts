import { Tensor4D } from '@tensorflow/tfjs-node';
import fs from 'node:fs/promises';
import { Sharp } from 'sharp';
import { getImageTensor } from './image.js';
import { TResolvedNetInput, computeFaceDescriptor } from '@vladmandic/face-api';

let euclideanDistance = (a: Float32Array, b: Float32Array) =>
	Math.hypot(...Object.keys(a).map((k) => b[Number(k)] - a[Number(k)]));

export type FaceDescriptor<T = Float32Array> = {
	label: string;
	data: T;
};

let _distanceThreshold = 0.6;
let _storedFaceDescriptors: FaceDescriptor[] = [];

export let loadFaceDescriptorStorage = async () => {
	let data = await fs.readFile('./models/face_recognition/data.json');
	let text = data.toString('ascii');
	let json = JSON.parse(text) as (FaceDescriptor & { data: any })[];
	_storedFaceDescriptors = json.map((faceDescriptor) => ({
		...faceDescriptor,
		data: new Float32Array(Object.values(faceDescriptor.data)),
	}));
	console.log(_storedFaceDescriptors);
};

export let getStoredFaceDescriptors = () => _storedFaceDescriptors;

let updateFaceDescriptorStorage = async () => {
	await fs.writeFile('./models/face_recognition/data.json', JSON.stringify(_storedFaceDescriptors));
};

export let storeFaceDescriptor = async (faceDescriptor: FaceDescriptor) => {
	_storedFaceDescriptors.push(faceDescriptor);
	await updateFaceDescriptorStorage();
};

export type DistancedFaceDescriptor = FaceDescriptor<Float32Array[]> & {
	distance: number;
};

export let findClosestFaceDescriptor = (faceDescriptor: FaceDescriptor): DistancedFaceDescriptor => {
	let descriptors = getStoredFaceDescriptors().map((fd) => ({
		...fd,
		distance: euclideanDistance(fd.data, faceDescriptor.data),
	}));
	let labels = [...new Set(descriptors.map((fd) => fd.label))];
	let combined: DistancedFaceDescriptor[] = labels
		.map((label) => {
			let fds = descriptors.filter((fd) => fd.label === label);
			let distance = fds.length > 0 ? fds.reduce((a, b) => a + b.distance, 0) / fds.length : 0;
			let descriptor: DistancedFaceDescriptor = {
				label,
				distance,
				data: fds.map((fd) => fd.data),
			};
			return descriptor;
		})
		.filter((dfd) => dfd.distance >= _distanceThreshold)
		.sort((a, b) => b.distance - a.distance);
	console.log(combined);
	return combined[0];
};

export let createFaceDescriptor = async (data: Tensor4D): Promise<FaceDescriptor> => {
	let descriptor = (await computeFaceDescriptor(data as unknown as TResolvedNetInput)) as Float32Array;
	return {
		label: 'unknown',
		data: descriptor,
	};
};

export let createFaceDescriptors = async (data: Tensor4D[]): Promise<FaceDescriptor[]> => {
	let descriptors = (await computeFaceDescriptor(data as unknown as TResolvedNetInput[])) as Float32Array[];
	return descriptors.map((descriptor) => ({
		label: 'unknown',
		data: descriptor,
	}));
};

export let createFaceDescriptorByImage = async (image: Sharp) => {
	let data = await getImageTensor(image);
	return createFaceDescriptor(data);
};

export let deleteStoredFaceDescriptors = async (label: string) => {
	_storedFaceDescriptors = _storedFaceDescriptors.filter((fd) => fd.label !== label);
	await updateFaceDescriptorStorage();
};
