# deepstack-coral-delegate

AI API with Deepstack API compatibility

### Inference types

-   CPU (`cpu`)
-   Google Coral (`edgetpu`)

## Models

| **Type**             | **Model**                | **Inference**    | **Modifiable** |
| -------------------- | ------------------------ | ---------------- | -------------- |
| Object Detection     | SSD MobileNet v2 (COCO)  | `cpu`, `edgetpu` | Yes            |
| Image Classification | EfficientNet (S)         | `cpu`, `edgetpu` | Yes            |
| Face Detection       | SSD MobileNet v2 (Faces) | `cpu`, `edgetpu` | Yes            |
| Face Recognition     | Face-API RecognitionNet  | `cpu`            | No             |

## Deepstack APIs

These are the currently supported Deepstack APIs:

### Deepstack Object Detection APIs

-   [x] `/v1/vision/detection` (Used by [`Blue Iris`](https://blueirissoftware.com))

### Deepstack Face Detection APIs

-   [x] `/v1/vision/face`

### Deepstack Face Recognition APIs

-   [x] `/v1/vision/face/register` (Used by [`Double Take`](https://github.com/jakowenko/double-take))
-   [x] `/v1/vision/face/recognize` (Used by [`Double Take`](https://github.com/jakowenko/double-take))
-   [ ] `/v1/vision/face/list`
-   [x] `/v1/vision/face/delete` (Used by [`Double Take`](https://github.com/jakowenko/double-take))
-   [ ] `/v1/vision/face/match`

### Deepstack Image Enchancement APIs

-   [ ] `/v1/vision/enhance`

### Deepstack Scene Recognition APIs

-   [x] `/v1/vision/scene`
